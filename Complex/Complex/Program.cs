﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Complex
{
    class Program
    {
        static void Main(string[] args)
        {
            string x = Console.ReadLine();
            string y = Console.ReadLine();
            Complex digit = new Complex { Re= Convert.ToDouble(x), Im= Convert.ToDouble(y) };
            Console.WriteLine(digit);
            Console.ReadKey();

        }
    }
    class Complex
    {
        public double Re { get; set; }
        public double Im { get; set; }

        public override string ToString()
        {
            return Re + "+j" + Im;
        }
    }
}
