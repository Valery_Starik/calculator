﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Calculation
{
    public class Class1
    {
        public class Complex
        {
            public double Re { get; set; }
            public double Im { get; set; }

            public override string ToString()
            {
                return Re + "+j" + Im;
            }
        }

        public static double addition(double x, double y)
        {
            double res = x + y;
            return (res);
        }
        public static string addition(double xRe, double yRe, double xIm, double yIm)
        {
            double resRe = xRe + yRe;
            double resIm = xIm + yIm;
            Complex digit = new Complex { Re = resRe, Im = resIm };
            return (Convert.ToString(digit));
        }

        public static double multiplication(double x, double y)
        {
            double res = x * y;
            return (res);
        }

        public static string multiplication(double xRe, double yRe, double xIm, double yIm)
        {
            double resRe = xRe * yRe - xIm*yIm;
            double resIm = xRe * yIm + xIm*yRe;
            Complex digit = new Complex { Re = resRe, Im = resIm };
            return (Convert.ToString(digit));
        }

        public static double division(double x, double y)
        {
            double res = x / y;
            return (res);
        }

        public static string division(double xRe, double yRe, double xIm, double yIm)
        {
            double resRe = (xRe*yRe+xIm*yIm)/(yRe*yRe+yIm*yIm);
            double resIm = (yRe*xIm-xRe*yIm)/(yRe * yRe + yIm * yIm);
            Complex digit = new Complex { Re = resRe, Im = resIm };
            return (Convert.ToString(digit));
        }

        public static double substraction(double x, double y)
        {
            double res = x - y;
            return (res);
        }

        public static string substraction(double xRe, double yRe, double xIm, double yIm)
        {
            double resRe = xRe - yRe;
            double resIm = xIm - yIm;
            Complex digit = new Complex { Re = resRe, Im = resIm };
            return (Convert.ToString(digit));
        }

    }
}
