﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Lib_console;
using Lib_operation;
using Calculation;

namespace Calc_prog
{
    class Complex_cl
    {
        public class Program
        {
            public static void vvod()
                {
            Operation Calc_complex = new Operation();
            Lib_operation.Operation.ShowMessage comp_method = Lib_console.Class1.data_output;
            double xRe = Convert.ToDouble(Lib_console.Class1.data_input_complex_Re());
            double xIm = Convert.ToDouble(Lib_console.Class1.data_input_complex_Im());
            double yRe = Convert.ToDouble(Lib_console.Class1.data_input_complex_Re());
            double yIm = Convert.ToDouble(Lib_console.Class1.data_input_complex_Im());

                Complex digit1 = new Complex { Re = xRe, Im = xIm };
                Calc_complex.Dict_comp(xRe, yRe, xIm, yIm, comp_method);
                
            }

        }
        public class Complex
        {
            public double Re { get; set; }
            public double Im { get; set; }

            public override string ToString()
            {
                return Re + "+j" + Im;
            }
        }
    }
}
