﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Calculation;
using Lib_console;

namespace Lib_operation
{
    public class Operation
    {
        public delegate void ShowMessage(string message);

        public void Dict(double x, double y, ShowMessage method)
        {
            Dictionary<string, string> Operation =
            new Dictionary<string, string>();

            Operation.Add("+", Convert.ToString(Calculation.Class1.addition(x, y)));
            Operation.Add("-", Convert.ToString(Calculation.Class1.substraction(x, y)));
            Operation.Add("*", Convert.ToString(Calculation.Class1.multiplication(x, y)));
            Operation.Add("/", Convert.ToString(Calculation.Class1.division(x, y)));

            string value = "";
            Operation.TryGetValue(Lib_console.Class1.operator_input(), out value);
            method(value);

        }
            public void Dict_comp(double xRe, double yRe, double xIm, double yIm, ShowMessage method)
        {
            Dictionary<string, string> Operation =
            new Dictionary<string, string>();
             
            Operation.Add("+", Convert.ToString(Calculation.Class1.addition(xRe, yRe, xIm, yIm)));
            Operation.Add("-", Convert.ToString(Calculation.Class1.substraction(xRe, yRe, xIm, yIm)));
            Operation.Add("*", Convert.ToString(Calculation.Class1.multiplication(xRe, yRe, xIm, yIm)));
            Operation.Add("/", Convert.ToString(Calculation.Class1.division(xRe, yRe, xIm, yIm)));

            string value = "";
            Operation.TryGetValue(Lib_console.Class1.operator_input(), out value);
            method(value);

        }
    }
}
