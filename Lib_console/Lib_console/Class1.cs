﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lib_console
{
    public class Class1
    {
        public static string Mode()
        {
            Console.WriteLine("Select the mode of the calculator: 1 - normal, 2 - complex number");
            return Console.ReadLine();
        }

        public static string data_input()
        {
            Console.WriteLine("Enter digit");
            return Console.ReadLine();
        }

        public static string data_input_complex_Re()
        {
            Console.WriteLine("enter the real part of complex number");
            string Re = Console.ReadLine();
            return Re;
        }

        public static string data_input_complex_Im()
        {
            Console.WriteLine("enter imaginary part of complex number");
            Console.Write("j");
            string Im = Console.ReadLine();
            return Im;
        }

        public static string operator_input()
        {
            Console.WriteLine("Enter operation");
            return Console.ReadLine();
        }

        public static string data_res(string message)
        {
            return message;
        }
        public  static void data_output(string message)
        {
            Console.Write("= " + message);
        }
    }
}
